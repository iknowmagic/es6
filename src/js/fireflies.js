export { fireflies }

function fireflies() {
  let options = {}, canvasId = 'fireflies', context, frfs = [];

  return {
    main: main
  }

  function init() {
    options = {
      width: window.innerWidth,
      height: window.innerHeight,
      canvas: document.getElementById(canvasId)
    };
    options.canvas.style.width = options.width+'px';
    options.canvas.style.height = options.height+'px';
    context = options.canvas.getContext('2d');
  }

  function main() {
    init();
    console.log(options);

  }


}
