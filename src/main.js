require('./styles/main.scss');
import {fireflies} from './js/fireflies';

document.addEventListener("DOMContentLoaded", () => {
  fireflies().main();
});
