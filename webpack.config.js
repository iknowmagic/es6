const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanupPlugin = require('webpack-cleanup-plugin');

const paths = {
  src: path.join(__dirname, 'src'),
  build: path.join(__dirname, 'build')
};

module.exports = {
  entry: ['./src/main'],
  output: {
    path: paths.build,
    filename: '[name].js'
  },
  devServer: {
    inline: true,
    contentBase: 'build',
    port: 9000
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loader: 'babel',
      query: {
        presets: ['es2015']
      }
    }, {
      test: /\.scss$/,
      loader: 'style-loader!css-loader!autoprefixer-loader!sass-loader!import-glob-loader'
    }, {
      test: /\.(png|jpg)$/,
      loader: 'url'
    }]
  },
  plugins: [
    new CleanupPlugin(),
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),

  ]
};
